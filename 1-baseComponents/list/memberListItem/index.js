import React, { Component } from 'react'
import { NativeModules, LayoutAnimation, Dimensions, StyleSheet } from 'react-native'
import { Button, View, Text, Body, Right, Left } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors } from '@cor/config'
import { TextInput } from '@cor/components'
import { observer } from 'mobx-react';

const { UIManager } = NativeModules

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true)

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

@observer
export default class MemberListItem extends Component {
    componentWillUpdate = () => {
      LayoutAnimation.easeInEaseOut()
    }

    focus = () => {
        this._input.focus()
    }

    render() {
        const { last, onPressDelete } = this.props

        const dynamicProps = !last
            ? {
                //value: item.name,  --> Value can't be controlled by this component, listForm will do it
                prefix: (color) => <Icon name={'account'} size={14} color={color} />,
                suffix: (color) => (
                    <Button
                        rounded
                        block
                        onPress={onPressDelete}
                        //transparent
                        style={styles.deleteButton}
                    >
                        <Icon name={'delete'} size={19} color={Colors.white} />
                    </Button>
                )
            } : {
                //placeholder: item.name, --> Placeholder can't be controlled by this component, listForm will do it
                prefix: (color) => <Icon name={'plus'} size={14} color={color} />
            }

        return (
            <View 
                style={styles.container}
            >
                <TextInput
                    ref={ref => this._input = ref}
                    maxLength={15}
                    contentContainerStyle={styles.textField}
                    mainBoxStyle={styles.mainBoxStyle}
                    value={this.props.value}
                    placeholder={this.props.placeholder}
                    onChangeText={this.props.onChangeText}
                    {...dynamicProps}
                    {...this.props}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: { 
        marginLeft: width*0.1,
        marginRight: 8,
        flexDirection: 'row',
    },
    textField: {
        marginBottom: 0,
    },
    mainBoxStyle: {
        borderBottomWidth: 0
    },
    deleteButton: { 
        width: width*0.1, 
        height: width*0.1
    }
})