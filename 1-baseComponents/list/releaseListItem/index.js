import React, { Component } from 'react'
import { Dimensions, StyleSheet } from 'react-native'
import { ListItem, View, Text, Body, Right, Left } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Initials } from '@cor/components'
import { Colors, Constants, ReleaseStrings as strings } from '@cor/config'
import { formatDate, replaceParams } from '@cor/config/helpers'
import Swipeout from 'react-native-swipeout'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class ReleaseListItem extends Component {
    render() {
        const { item, swipeoutFunctions } = this.props
        const left = [
            {
                text: <Icon name={'delete'} size={30} color={Colors.text} />,
                onPress: () => swipeoutFunctions.deleteItem(item),
                backgroundColor: Colors.redDeny
            }
        ]

        let backgroundColor = Colors.primary
        switch (item.status) {
            case 'released':
                backgroundColor = Colors.greenAgree
                break
            case 'unrelease':
                backgroundColor = Colors.redDeny
                break
        }

        const args = [
            formatDate(item.date, false)
        ]

        return (
            <Swipeout
                autoClose={true}
                backgroundColor={Colors.background}
                left={left}
            >
                <ListItem thumbnail {...this.props}>
                    <Left>
                        <Initials 
                            size={1.3}
                            iconName={Constants.statusIconMap[item.status]}
                            backgroundColor={backgroundColor}
                        />
                    </Left>
                    <Body>
                        <View style={styles.body}>
                            <Text style={{ color: Colors.text }}>{replaceParams(strings.releaseInfo, args)}</Text>
                        </View>
                    </Body>
                    <Right>
                        <Icon name={'chevron-right'} size={30} color={Colors.primary} />
                    </Right>
                </ListItem>
            </Swipeout>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: '10%',
    }
})
