import React, { Component } from 'react'
import { NativeModules, LayoutAnimation, Dimensions, StyleSheet } from 'react-native'
import { ListItem, View, Text, Body, Right, Left } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors } from '@cor/config'
import Swipeout from 'react-native-swipeout'

const { UIManager } = NativeModules

UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true)

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class ProjectListItem extends Component {
    componentWillUpdate = () => {
        LayoutAnimation.easeInEaseOut()
    }

    render() {
        const { item, swipeoutFunctions } = this.props
        const left = [
            {
                text: <Icon name={'pencil'} size={30} color={Colors.text} />,
                onPress: () => swipeoutFunctions.editItem(item),
                backgroundColor: Colors.secondarybg
            },
            {
                text: <Icon name={'delete'} size={30} color={Colors.text} />,
                onPress: () => swipeoutFunctions.deleteItem(item),
                backgroundColor: Colors.redDeny
            }
        ]

        return (
            <Swipeout
                autoClose={true}
                backgroundColor={Colors.background}
                left={left}
            >
                <ListItem thumbnail {...this.props}>
                    <Left>
                        <View style={styles.thumbnailView}>
                            <Text style={styles.thumbnail}>{item.name.charAt(0).toUpperCase()}</Text>
                        </View>
                    </Left>
                    <Body>
                        <View style={styles.body}>
                            <View>
                                <Text style={styles.mainText}>{item.name}</Text>
                                <Text style={styles.subText}>{item.desc}</Text>
                            </View>
                            <View style={[styles.body, { marginRight: '5%' }]}>
                                <Icon name={'account'} size={15} color={Colors.text} />
                                <Text style={[styles.subText, { marginLeft: '2%' }]}>
                                    {item.members ? item.members.length : 0}
                                </Text>
                            </View>
                        </View>
                    </Body>
                    <Right>
                        <Icon name={'chevron-right'} size={15} color={Colors.primary} />
                    </Right>
                </ListItem>
            </Swipeout>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    mainText: {
        fontWeight: '400',
        fontSize: width * 0.0375,
        color: Colors.text
    },
    subText: {
        fontSize: width * 0.028,
        color: Colors.text,
        opacity: 0.75
    },
    thumbnailView: {
        backgroundColor: Colors.primary,
        width: width * 0.11,
        height: width * 0.11,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 200,
    },
    thumbnail: {
        fontWeight: 'bold',
        fontSize: width * 0.065,
        color: Colors.white
    }
})