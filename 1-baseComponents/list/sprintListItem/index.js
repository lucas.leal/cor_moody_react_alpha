import React, { Component } from 'react'
import { NativeModules, LayoutAnimation, Dimensions, StyleSheet } from 'react-native'
import { ListItem, View, Text, Body, Right, Left } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Initials } from '@cor/components'
import { Colors, Constants, SprintStrings as strings } from '@cor/config'
import { formatDate, replaceParams } from '@cor/config/helpers'
import Swipeout from 'react-native-swipeout'

const { UIManager } = NativeModules

UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true)

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class SprintListItem extends Component {
    componentWillUpdate = () => {
        LayoutAnimation.easeInEaseOut()
    }

    render() {
        const { item, swipeoutFunctions } = this.props
        const left = [
            {
                text: <Icon name={'delete'} size={30} color={Colors.text} />,
                onPress: () => swipeoutFunctions.deleteItem(item),
                backgroundColor: Colors.redDeny
            }
        ]

        let backgroundColor = Colors.primary
        let textColor = Colors.text
        switch (item.status) {
            case 'completed':
                backgroundColor = Colors.greenAgree
                textColor = Colors.greenAgree
                break
            case 'cancelled':
                backgroundColor = Colors.redDeny
                textColor = Colors.redDeny
                break
        }

        const argsLineOne = [
            ''+(item.id+1)
        ]

        const argsLineTwo = [
            ''+item.completedDays,
            ''+item.totalDays
        ]

        return (
            <Swipeout
                autoClose={true}
                backgroundColor={Colors.background}
                left={left}
            >
                <ListItem
                    thumbnail
                    {...this.props}
                >
                    <Left>
                        <Initials
                            size={1.1}
                            iconName={Constants.statusIconMap[item.status]}
                            backgroundColor={backgroundColor}
                        />
                    </Left>
                    <Body>
                        <View style={styles.left}>
                            <Text style={styles.mainText}>
                                {replaceParams(strings.sprintInfo, argsLineOne)}
                            </Text>
                            <View style={styles.leftView}>
                                <Icon name={'calendar-blank'} size={15} color={textColor}/>
                                <Text style={[styles.subText, { color: textColor }]}>
                                    {replaceParams(strings.sprintDailyCount, argsLineTwo)}
                                </Text>
                            </View>
                        </View>
                    </Body>
                    <Right>
                        <Icon name={'chevron-right'} size={15} color={Colors.primary} />
                    </Right>
                </ListItem>
            </Swipeout>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: '-10%',
    },
    left: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    leftView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    mainText: {
        color: Colors.text,
        fontSize: height * 0.025
    },
    subText: {
        //color: Colors.placeHolder,
        marginLeft: 4
    },
    thumbnailView: {
        //backgroundColor: Colors.primary,
        width: width * 0.11,
        height: width * 0.11,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 200,
    },
})