import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Dimensions, StyleSheet, View, Text } from 'react-native'
import { Colors } from '@cor/config'
import { Button } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { set } from 'mobx';

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

@observer
export default class MemberListCard extends Component {
    
    IconButton = ({ onPress, name, label, selected, disabled, style, light, dark, size, buttonStyles }) => (
        <View style={style}>
            {label && label != '' &&
                <View style={styles.buttonRowHeader}>   
                    <Text style={styles.labelText}>{label}</Text>
                </View>
            }
            <Button
                rounded
                light={!selected && light}
                dark={!selected && dark}
                block
                onPress={onPress}
                style={[
                    styles.button,
                    buttonStyles
                ]}
                disabled={disabled}
            >
                <Icon name={name} size={size || 28} color={Colors.text} />
            </Button>
        </View>
    )

    ButtonRow = ({children, label, disabled}) => (
        <View>
            {disabled &&
                <View 
                    style={styles.disabledButtonRow}
                />
            }
            <View style={{marginTop: height*0.02}}>
                {label && label != '' &&
                    <View style={styles.buttonRowHeader}>   
                        <Text style={styles.labelText}>{label}</Text>
                    </View>
                }
                <View 
                    style={styles.buttonRow}
                >
                    {children}
                </View>
            </View>
        </View>
    )

    renderSelectOneRow = (buttonRow, item, key) => {
        return (
            <this.ButtonRow 
                key={key}
                paddingFactor={buttonRow.buttons.length} 
                label={buttonRow.label}
                disabled={buttonRow.disabled(item.id)}
            >
                {buttonRow.buttons.map(button => 
                    <this.IconButton
                        key={button.name}
                        onPress={() => buttonRow.onPress(buttonRow.id, button.id, item.id)} 
                        name={button.name} 
                        label={button.label}
                        selected={buttonRow.selected(buttonRow.id, button.id, item.id)}
                        disabled={buttonRow.disabled(item.id)}
                        light
                    /> 
                )}
            </this.ButtonRow>
        )
    }

    renderMultiSelectRow = (buttonRow, item, key) => {
        return (
            <this.ButtonRow
                key={key}
                paddingFactor={buttonRow.buttons.length} 
                label={buttonRow.label}
                disabled={buttonRow.disabled(item.id)}
            >
                {buttonRow.buttons.map(button => 
                    <this.IconButton
                        key={button.name}
                        onPress={() => button.onPress(button.id, item.id, button.activeStat, button.deactStat)} 
                        name={button.name} 
                        label={button.label} 
                        selected={button.selected(button.id, item.id, button.activeStat, button.deactStat)}
                        disabled={buttonRow.disabled(item.id)}
                        light
                    /> 
                )}
            </this.ButtonRow>
        )
    }

    renderButtonRow = (buttonRow, item, key) => {
        switch (buttonRow.type) {
            case 'select-one': return this.renderSelectOneRow(buttonRow, item, key)
            case 'select-multiple': return this.renderMultiSelectRow(buttonRow, item, key)
            default: return <View />
        }
    }

    render() {
        const { item, buttonsData, functions } = this.props

        return (
            <View
                style={styles.container}
            >
                <View
                    style={styles.cardHeader}
                >
                    <Text style={styles.headerText}>
                        {item.name}
                    </Text>
                    <View style={{ position: 'absolute', width: '100%' }}>
                        <this.IconButton
                            onPress={() => functions.setPTO(item.id)} 
                            name={'briefcase'} 
                            selected={item.pto}
                            style={{alignSelf: 'flex-end'}}
                            size={20}
                            buttonStyles={{width: width*0.1, height: width*0.1 }}
                            dark
                        /> 
                    </View>
                </View>
                {buttonsData.map((buttonRow, index) => this.renderButtonRow(buttonRow, item, 'btn'+index))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.secondarybg,
        marginHorizontal: 15,
        marginVertical: 15,
        borderRadius: 20,
        overflow: 'hidden'
    },
    labelText: {
        color: Colors.text,
        fontWeight: 'bold',
        fontSize: width*0.042
    },
    headerText: {
        color: Colors.text,
        fontWeight: 'bold',
        fontSize: width*0.05
    },
    cardHeader: {
        height: width*0.15,
        backgroundColor: Colors.tertiarybg,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 7,
        paddingHorizontal: 10
    },
    buttonRowHeader: {
        paddingTop: height*0.02,
        paddingBottom: height*0.015,
        alignItems: 'center',
        justifyContent: 'center'
    },  
    buttonRow: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        paddingBottom: height*0.045,  
    },
    disabledButtonRow: {
        backgroundColor: Colors.tertiarybg,
        opacity: 0.75,
        position: 'absolute',
        width: '100%',
        height: '100%',
        flex: 1,
    },
    button: { 
        alignSelf: 'center',
        width: width*0.13, 
        height: width*0.13,
        elevation: 0,
        shadowColor: 'transparent',
    }
})