import React, { Component } from 'react'
import { Dimensions, StyleSheet } from 'react-native'
import { ListItem, View, Text, Body, Right, Left } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Initials } from '@cor/components'
import { Colors, Constants, DailyStrings as strings } from '@cor/config'
import { formatDate, replaceParams } from '@cor/config/helpers'
import Swipeout from 'react-native-swipeout'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class DailyListItem extends Component {

    render() {
        const { item, swipeoutFunctions } = this.props

        let backgroundColor = Colors.primary
        let buttonColor = Colors.redDeny
        let iconName = 'close'
        let execute = (item) => swipeoutFunctions.cancelDaily(item)
        switch (item.status) {
            case 'completed':
                backgroundColor = Colors.greenAgree
                break
            case 'cancelled':
                backgroundColor = Colors.redDeny
                buttonColor = Colors.secondarybg
                iconName = 'rotate-left'
                execute = (item) => swipeoutFunctions.reopenDaily(item)
                break
        }

        const isOngoing = item.status == 'ongoing'
        const left = item.status == 'completed'
            ? undefined
            : [
                {
                    text: <Icon name={iconName} size={30} color={Colors.text} />,
                    onPress: () => execute(item),
                    backgroundColor: buttonColor
                }
            ]

        const args = [
            ''+(item.id+1),
            formatDate(item.date, true)
        ]

        return (
            <Swipeout
                autoClose={true}
                backgroundColor={Colors.background}
                left={left}
            >
                <ListItem thumbnail {...this.props}>
                    <Left>
                        <Initials
                            size={1.1}
                            iconName={Constants.statusIconMap[item.status]}
                            backgroundColor={backgroundColor}
                        />
                    </Left>
                    <Body>
                        <Text style={styles.mainText}>{replaceParams(strings.dailyInfo, args)}</Text>
                    </Body>
                    <Right>
                        <Icon name={'chevron-right'} size={30} color={Colors.primary} />
                    </Right>
                </ListItem>
            </Swipeout>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    mainText: {
        fontWeight: '400',
        fontSize: width * 0.0375,
        color: Colors.text
    },
    subText: {
        fontSize: width * 0.028,
        color: Colors.text,
        opacity: 0.75
    },
    thumbnailView: {
        //backgroundColor: Colors.primary,
        width: width * 0.09,
        height: width * 0.09,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 200,
    },
    thumbnail: {
        fontWeight: 'bold',
        fontSize: width * 0.045,
        color: Colors.white
    }
})