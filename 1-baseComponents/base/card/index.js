import React, { Component } from 'react'
import { View, Text } from 'react-native'

export default class Card extends Component {
    render() {
        return (
            <View
                style={{ paddingHorizontal: 12, paddingVertical: 20 }}
            >
                {this.props.children}
            </View>
        )
    }
}
