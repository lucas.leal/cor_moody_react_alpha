import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors } from '@cor/config'

width = Math.round(Dimensions.get('window').width)
height = Math.round(Dimensions.get('window').height)

export default class Initials extends Component {
    render() {
        const {
            size,
            label,
            iconName,
            contentContainerStyle,
            style,
            backgroundColor
        } = this.props

        if (typeof label == 'string') {
            initials = label.length > 1 
                ? label.charAt(0).toUpperCase()+label.charAt(1).toUpperCase()
                : label.charAt(0).toUpperCase()
        }

        return (
            <View
                style={[
                    styles.container,
                    {
                        width: width*0.1*size,
                        height: width*0.1*size,
                        backgroundColor: backgroundColor
                    },
                    contentContainerStyle
                ]}
            >
                {label
                    ? (
                        <Text
                            numberOfLines={1}
                            style={[
                                styles.text,
                                {
                                    fontSize: width*0.05*(size-0.2),
                                },
                                style
                            ]}
                        >
                            {initials}
                        </Text>
                    ) : (
                        <Icon name={iconName} size={30} color={Colors.text} />
                    )
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 200,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: Colors.white,
        fontWeight: '400',
    }
})