import React, { Component } from 'react';
import { Switch as NBSwitch } from 'native-base'
import { Dimensions, StyleSheet, View, Text } from 'react-native';
import { Colors } from '@cor/config'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class Switch extends Component {
    render() {
        const {
            ref,
            label,
            type,
            message,
        } = this.props

        let color
        switch(type) {
            case 'error':
                color = Colors.redDeny
                break;
            case 'success':
                color = Colors.greenAgree
                break;
            case 'default':
            default:
                color = Colors.black
                break;
        }
        //color+='99'
        colorFade = color+'99'

        return (
            <View style={styles.container}>
                <View>
                    {/* Label */}
                    <View style={[styles.mainBoxStyle, {borderBottomColor: colorFade} ]} >
                        {/* Prefix */}
                        <Text style={[styles.textStyle, this.props.textStyle, {color: colorFade}]}>{label}</Text>
                        {/* Suffix */}
                        <NBSwitch 
                            { ...this.props}
                        />
                    </View>
                </View>
                {/* Error message */}
                <Text style={[styles.textStyle, this.props.textStyle, {color: color}]}>{message}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom: 20
    }, 
    mainBoxStyle: {
        //borderBottomWidth: 0.85,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width: '100%'
    },
    textStyle: {
        fontSize: height*0.019,
        alignContent: 'flex-end',
        marginLeft: width*0.015
    },
    textBoxStyle: {
        marginLeft: width*0.02,
        marginRight: width*0.02,
        flex: 1
    },
    errorMessage: {
        color:Colors.redDeny, 
        marginLeft: width*0.02,
        fontSize: height*0.017,
    }
})