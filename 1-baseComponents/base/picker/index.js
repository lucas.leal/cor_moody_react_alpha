import React, { Component } from 'react'
import { Dimensions, StyleSheet, View, Text } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { Colors } from '@cor/config'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class Picker extends Component {
    render() {
        const {
            label,
            type,
            message,
            prefix,
            suffix,
            items
        } = this.props

        let color
        let feedback
        switch(type) {
            case 'error':
                color = Colors.redDeny
                feedback = <Icon name="close" size={12} color={Colors.redDeny} />
                break;
            case 'success':
                color = Colors.greenAgree
                feedback = <Icon name="check    " size={12} color={Colors.greenAgree} />
                break;
            case 'default':
            default:
                color = Colors.placeholder
                break;
        }
        //color+='99'
        colorFade = color+'99'

        return (
            <View style={styles.container}>
                <View>
                    {/* Label */}
                    <Text style={[styles.textStyle, this.props.textStyle, {color: colorFade}]}>{label}</Text>
                    <View style={[styles.mainBoxStyle, {borderBottomColor: colorFade} ]} >
                        {/* Prefix */}
                        {prefix && 
                            <View style={{ marginLeft: width*0.02, marginBottom: 3 }}>{prefix}</View>
                        }
                        {/* Text Box */}
                        <View style={styles.textBoxStyle}>
                            <RNPickerSelect
                                ref={picker => this._picker = picker}
                                style={{ width: '100%' }}
                                useNativeAndroidPickerStyle={false}
                                textInputProps={{style: {color: Colors.text}}}
                                mode={'dropdown'}
                                { ...this.props}
                            />
                        </View>
                        <View
                            style={styles.suffixContainer}
                        >
                            {/* Feedback */}
                            {feedback &&
                                <View>{feedback}</View>
                            }
                            {/* Suffix */}
                            {suffix &&
                                <View style={{paddingLeft: width*0.02}}>{suffix}</View>
                            }
                        </View>
                    </View>
                </View>
                {/* Error message */}
                <Text style={[styles.textStyle, this.props.textStyle, {color: color}]}>{message}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom: 20
    }, 
    mainBoxStyle: {
        borderBottomWidth: 0.85,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width: '100%'
    },
    textStyle: {
        width: '100%',
        fontSize: height*0.017,
        alignContent: 'flex-end',
        marginLeft: width*0.015
    },
    textBoxStyle: {
        marginLeft: width*0.02,
        marginRight: width*0.02,
        flex: 1
    },
    errorMessage: {
        color:Colors.redDeny, 
        marginLeft: width*0.02,
        fontSize: height*0.017,
    },
    suffixContainer: { 
        marginRight: width*0.02, 
        marginBottom: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})