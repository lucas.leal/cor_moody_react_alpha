import React, { Component } from 'react';
import { Platform, Dimensions, StyleSheet, View, Text, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors } from '@cor/config'


width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

export default class CorTextInput extends Component {
    state = {
        focused: false
    }

    focus = () => {
        this._input.focus()
    }
    
    _onFocus = () => {
        this.setState({ focused: true })
        if (this.props.onFocus) this.props.onFocus()
    }

    _onEndEditing = () => {
        this.setState({ focused: false })
        if (this.props.onEndEditing) this.props.onEndEditing()
    }

    render() {
        const {
            label,
            type,
            message,
            prefix,
            suffix,
        } = this.props

        let color
        let feedback
        switch(type) {
            case 'error':
                color = Colors.redDeny
                feedback = <Icon name="close" size={12} color={Colors.redDeny} />
                break;
            case 'success':
                color = Colors.greenAgree
                feedback = <Icon name="check" size={12} color={Colors.greenAgree} />
                break;
            case 'default':
            default:
                if (this.state.focused) color = Colors.link
                else color = Colors.placeholder
                break;
        }
        //color+='99'
        colorFade = color+'99'

        let fontWeight = this.state.focused
            ? 'bold'
            : 'normal'

        return (
            <View
                style={[
                    styles.wraper,
                    this.props.contentContainerStyle
                ]}
            >
                <View style={[
                    styles.container
                ]}>
                    <View>
                        {/* Label */}
                        {label !== '' &&
                            <Text 
                                style={[
                                    styles.textStyle, 
                                    this.props.textStyle, 
                                    {
                                        color: colorFade,
                                        fontWeight: fontWeight
                                    }
                                ]}
                            >
                                {label}
                            </Text>
                        }
                        <View 
                            style={[
                                styles.mainBoxStyle, 
                                {
                                    borderColor: colorFade,
                                    borderBottomWidth: this.state.focused
                                        ? 2
                                        : 0.85
                                },
                                this.props.mainBoxStyle
                            ]}
                        >
                            {/* Prefix */}
                            {prefix && 
                                <View style={{ marginLeft: width*0.02, /*marginBottom: 7*/ }}>
                                    {prefix(color)}
                                </View>
                            }
                            {/* Text Box */}
                            <View 
                                style={[
                                    styles.textBoxStyle,
                                    this.props.textBoxStyle
                                ]}
                            >
                                <TextInput
                                    ref={input => this._input = input}
                                    style={styles.textInput}
                                    onFocus={this._onFocus}
                                    onEndEditing={this._onEndEditing}
                                    maxHeight={height*0.052}
                                    autoGrow={false}
                                    placeholderTextColor={Colors.placeholder}
                                    { ...this.props}
                                />
                            </View>
                            {(feedback || suffix ) &&
                                <View
                                    style={styles.suffixContainer}
                                >
                                    {/* Feedback */}
                                    {feedback &&
                                        <View>{feedback}</View>
                                    }
                                    {/* Suffix */}
                                    {suffix &&
                                        <View style={{paddingLeft: width*0.02}}>
                                            {suffix(color)}
                                        </View>
                                    }
                                </View>
                            }
                        </View>
                    </View>
                    {/* Error message */}
                    {message !== '' &&
                        <Text 
                            style={[
                                styles.textStyle, 
                                this.props.textStyle, 
                                {
                                    color: color,
                                    fontWeight: fontWeight
                                }
                            ]}
                        >
                            {message}
                        </Text>
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wraper: {
        flex: -1,
        flexDirection: 'row',
        marginBottom: height*0.03,
        width: '100%'
    },
    container: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '100%' 
    }, 
    mainBoxStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: height*0.05,
        flex: 1,
    },
    textStyle: {
        fontSize: height*0.018,
        alignContent: 'flex-end',
        marginLeft: width*0.015,
        marginTop: 4
    },
    textBoxStyle: {
        flex: 1,
        marginLeft: width*0.02,
        marginRight: width*0.02,
        height: height*0.05,
    },
    textInput: {
        /*marginBottom: Platform.OS === 'ios'
            ? 7
            : -11,*/
        //height: height*0.05 ,
        //backgroundColor: '#FF0000',
        color: Colors.text
    },
    errorMessage: {
        color:Colors.redDeny, 
        marginLeft: width*0.02,
        fontSize: height*0.017,
    },
    suffixContainer: { 
        marginRight: width*0.02, 
        //marginBottom: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})