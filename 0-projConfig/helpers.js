import React from 'react'
import { Platform, Dimensions } from 'react-native'
import { View, Button } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors, Constants } from '@cor/config'

export function log (args) {
    const PREFIX = '--------------------->  '
    if (Array.isArray(args)) {
        args.forEach(arg => {
            if (typeof arg == 'object') {
                for (const [key, value] of Object.entries(arg)) {
                    console.log(PREFIX + key + ': ' + value);
                }
            }
            else {
                console.log(PREFIX + arg)
            }
        })
    }
    else {
        if (typeof args == 'object') {
            for (const [key, value] of Object.entries(args)) {
                console.log(PREFIX + key + ': ' + value);
            }
        }
        else {
            console.log(PREFIX + args)
        }
    }
}

export function createLaterDate (today, monthsToSkip) {
    return new Date(today.getFullYear(), today.getMonth()+monthsToSkip, today.getDate())
}

export function formatDate (date, showDay) {
    const {
        dateFormat,
        dateFormatM
    } = Constants
    if (!(date instanceof Date)) date = new Date(date)
    const day = date.getDate()
    const month = date.getMonth()+1
    const year = date.getFullYear()
    return showDay
        ? dateFormat.replace('dd', day).replace('mm', month).replace('yyyy', year)
        : dateFormatM.replace('mm', month).replace('yyyy', year)
}

export function replaceParams (string = '', args = []) {
    let newString = string
    for (let i = 0; i < args.length; i++) {
        newString = newString.replace(`{${i}}`, args[i])
    }
    return newString
}

export function baseOpts (navigation, title, removeBack = false, removeBackground = false) {
    const size = Dimensions.get('screen').width*0.1
    const IconButton = (props) => (
        <Button
            rounded
            transparent
            style={{
                paddingHorizontal: 14,
                paddingVertical: 4.5,
            }}
            {...props}
        >
            <Icon name={props.name} size={size} color={Colors.white} />
        </Button>
    )

    const args = navigation.getParam('args', [])

    return {
        headerStyle: {
            backgroundColor: removeBackground ? Colors.background : Colors.primary,
            shadowColor: 'transparent',
            elevation: 0,
            borderBottomWidth: 0,
            height: Platform.OS === 'ios' 
                ? Constants.iosHeaderSize 
                : Constants.androidHeaderSize,
        },
        headerTitle: replaceParams(title, args) || '',
        headerTintColor: Colors.white,
        headerLeft: removeBack 
            ? null
            : (
                <View style={{ flexDirection: 'row', marginRight: 9 }}>
                    <IconButton onPress={() => navigation.goBack()} name='chevron-left' />
                </View>
            )
    }
}