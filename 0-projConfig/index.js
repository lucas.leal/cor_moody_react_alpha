import strings from './strings.json'
import constants from './constants.json'
import dummy from './dummy.json'

// General exports
export { default as Colors } from './colors.json'
export const Locale = constants.Locale
export const AppName = constants.AppName
export const Constants = constants.Constants

// Screen strings
export const DatePickerStrings = strings.DatePickerStrings
export const DailyStrings = strings.DailyStrings
export const DayStrings = strings.DayStrings
export const ProjectStrings = strings.ProjectStrings
export const ReleaseStrings = strings.ReleaseStrings
export const SprintStrings = strings.SprintStrings
export const UserStrings = strings.UserStrings

// Placeholder data
export const DailyDummy = dummy.DailyDummy
export const DayDummy = dummy.DayDummy
export const ProjectDummy = dummy.ProjectDummy
export const SprintDummy = dummy.SprintDummy