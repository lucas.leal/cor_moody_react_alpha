import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Dimensions, StyleSheet, View, Text } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { Colors, DailyStrings } from '@cor/config'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

@observer
export default class CarouselList extends Component {
    
    state = {
        activeIndex: 0
    }

    renderItem = ({item, index}) => {
        const { 
            RenderComponent,
            functions,
            buttonsData
        } = this.props
        return (
            <RenderComponent
                item={item}
                buttonsData={buttonsData}
                functions={functions}
                {...item.props}
                {...buttonsData.props}
            />
        )
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.background }}>
                <Carousel
                    ref={ref => { this._carousel = ref }}
                    renderItem={({item, index}) => this.renderItem({item, index})}
                    onSnapToItem={(index) => this.setState({ activeIndex: index }) }
                    sliderWidth={width}
                    itemWidth={width}
                    //sliderHeight={height*2}
                    //itemHeight={height*2}
                    {...this.props}
                />
                {this.props.pagination &&
                    <Pagination 
                        dotsLength={this.props.data.length}
                        activeDotIndex={this.state.activeIndex}
                        dotColor={Colors.primary}
                        inactiveDotColor={Colors.text}
                        dotStyle={styles.dotStyle}
                    />
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        alignItems: 'center', 
        width: '100%',
    },
    cardItemStyle: {
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        width: '85%'
        //marginHorizontal: width*0.05
    },
    cardStyle: {
        width: width*0.9
    },  
    listItemStyle: {
        flexDirection: 'column',
    },
    labelStyle: {
        color: Colors.primary,
    }, 
    titleStyle: {
        color: Colors.greenAgree,
        fontSize: height*0.032
    },
    dotStyle: {
        width: width*0.02,
        height: width*0.02
    }
})