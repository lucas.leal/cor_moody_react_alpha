import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import {
    Platform,
    Dimensions, 
    FlatList, 
    TouchableWithoutFeedback,
    BackHandler,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native'
import { ListItem, View, Text, Body, Right, Left  } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Colors } from '@cor/config'
import { TextInput } from '@cor/components'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

@observer
export default class ListForm extends Component {
    state = {
        createdNewField: false
    }

    onFocusLast = () => {
        const { 
            data,
            newItemObject
        } = this.props

        // -1 because it's Members length + 1 (New member). If 5 members this will be 6, last index is 5
        const listLength = data.length
        data.push(newItemObject(listLength))
        this.setState({ createdNewField: true })
        //console.log([].concat(this.props.data.slice()))
    }

    onChangeText = (item, value) => {
        if (!item.last) {
            const { data } = this.props

            data[item.id].value = value
            //this.forceUpdate()
        }
        //console.log([].concat(this.props.data.slice()))
    }

    onPressDelete = (id) => {
        const { data } = this.props
        data.splice(id, 1)
        for (let i = 0; i < data.length; i++) data[i].id = i
    }

    componentDidUpdate = () => {
        if (this._lastField && this.state.createdNewField) {
            this._lastField.focus()
            this.setState({ createdNewField: false })
            setTimeout(() => this._scrollView.scrollToEnd(), 2)
        }
    }
    
    renderItem = ({item, index}) => {
        const { 
            RenderComponent,
            data
        } = this.props

        //console.log(`Index: ${index}    ID: ${item.id}   Value: ${item.value}`)
        //if (!item.last) console.log(`Value Store:  ${formFields[item.id].value}`)

        return (
            <RenderComponent
                ref={ref => {
                    if (item.id == data.length-1) this._lastField = ref
                }}
                key={item.id}
                value={data[item.id].value}
                item={item}
                onChangeText={(value) => this.onChangeText(item, value)}
                onPressDelete={() => this.onPressDelete(item.id)}
                {...item.props}
            />
        )
    }
    /*
    updateFormFields = () => {
        const {
            informationList,
            formFields
        } = this.props

        informationList.forEach(item => {
            formFields.push(item)
        })
    }

    componentWillMount = () => {
        const { informationList } = this.props
        informationList.push(this._lastItem(informationList.length))
        this.updateFormFields()
    }*/

    render() {
        const { 
            RenderComponent, 
            newValueString 
        } = this.props

        /*check = {
            data: new Map(this.props.data),
            state: this.state
        }*/

        return (
            <KeyboardAvoidingView
                behavior={
                    Platform.OS === 'ios' 
                        ? 'padding'
                        : undefined
                }
                style={{ flex: 1, flexDirection:'row', alignItems:'flex-start', backgroundColor: Colors.background }}
            >
                <ScrollView
                    ref={ref => this._scrollView = ref}
                    keyboardShouldPersistTaps={'handled'}
                    bounces={false}
                    style={{flex:1}}
                >
                    <View
                        style={{
                            marginTop: height*0.03,
                            marginHorizontal: width*0.035,
                            paddingBottom: height*0.01,
                            flexDirection: 'row'
                        }}
                    >
                        <TextInput
                            prefix={(color) => <Icon name={'magnify' } size={20} color={color} />}
                            placeholder={'Search... (Not functional yet)'}
                        />
                    </View>
                    {/*
                    <FlatList
                        extraData={check}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        bounces={false}
                        ListFooterComponent={ListFooterComponent}
                        { ...this.props}
                    />
                    */}
                    {
                        this.props.data.map(item => this.renderItem({item: item, index: 0}))
                    }
                    <RenderComponent
                        placeholder={newValueString}
                        onFocus={this.onFocusLast}
                        last={true}
                    />
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}