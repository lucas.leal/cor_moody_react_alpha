import React, { Component } from 'react'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import { 
    Dimensions, 
    FlatList
} from 'react-native'
import { View } from 'native-base'
import { FloatingAction } from 'react-native-floating-action'
import { Colors } from '@cor/config'

width = Math.round(Dimensions.get('screen').width)
height = Math.round(Dimensions.get('screen').height)

@observer
export default class ProjectList extends Component {
/*
    handleBackPress = () => {
        const {
            handleBackPress = () => {}
        } = this.props
        
        if (this.state.selectedIndex !== -1) this.setState({ selectedIndex: -1 })
        else handleBackPress()
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }*/

    renderItem = ({item, index}) => {
        const { 
            RenderComponent,
            swipeoutFunctions,
            onListPress = () => {}
        } = this.props

        return (
            <RenderComponent 
                item={item}
                button
                noIndent
                noBorder
                onPress={() => onListPress(item)}
                swipeoutFunctions={swipeoutFunctions}
                style={{ backgroundColor: 'transparent' }}
                {...item.props}
            />
        )
    }

    render() {
        const { 
            showFab,
            floatingActionProps
        } = this.props

        check = toJS(this.props.data)
        
        return (
            <View style={{ flex: 1, backgroundColor: Colors.background }}>
                <FlatList
                    extraData={check}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    bounces={false}
                    { ...this.props}
                />
                {showFab &&
                    <FloatingAction 
                        {...floatingActionProps}
                    />
                }
            </View>
        )
    }
}