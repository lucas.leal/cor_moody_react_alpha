import React, { Component } from 'react'
import {
    StyleSheet,
    TouchableWithoutFeedback,
    ActivityIndicator,
    ScrollView,
    KeyboardAvoidingView,
    Platform
} from 'react-native'
import { observer, toJS } from 'mobx-react'
import {
    Text,
    Button,
    View
} from 'native-base'
import { 
    TextInput, 
    DatePicker, 
    Switch,
    Card,
    Picker
} from '@cor/components'
import {
    Colors,
    DatePlaceholder,
    Locale,
} from '@cor/config'
import { formatDate } from '@cor/config/helpers'

@observer
export default class FormScreen extends Component {

    componentWillMount = () => {
        //this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
        //this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

        const { 
            informationList, 
            formFields
        } = this.props

        informationList.forEach(object => {
            formFields[object.id].value = object.value || ''
            formFields[object.id].errorMessage = ''
        })
    }

    componentDidMount = () => {
        this.validate()
    }

    /*
    componentWillUnmount = () => {
        this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener.remove()
    }

    keyboardDidShow(e) {
        let size = Dimensions.get('window').height * 0.9 - e.endCoordinates.height
        this.setState({
            showSpacer: true,
            spacerSize: size,
            paddingSize: e.endCoordinates.height * 0.0001
        })
    }

    keyboardDidHide(e) {
        screenHeight = height
        this.setState({
            showSpacer: false,
            spacerSize: '100%',
            paddingSize: 0
        })
    }*/

    validate = () => {
        const { informationList, formFields, formState } = this.props
        formState.enableSubmit = true
        informationList.forEach(object => {
            if (!object.ignoreValidation) {
                if (formFields[object.id].value == '' && !object.allowEmpty) formState.enableSubmit = false
                if (formFields[object.id].errorMessage.length > 0) formState.enableSubmit = false
                if (object.mutualEmpty) {
                    if (formFields[object.mutualEmpty].value != '' && formFields[object.id].value == '') formState.enableSubmit = false
                }
            }
        })
    }

    onBlur = () => {
        this.validate()
    }

    onChangeText = (object, value) => {
        const { formFields } = this.props
        formFields[object.id].value = value
        formFields[object.id].errorMessage = formFields[object.id].validate()
        this.validate()
    }

    formatDate = (date, object) => {
        if (!(date instanceof Date)) date = new Date(date)
        return formatDate(date, object.showDay)
    }

    onDateChange = (object, value) => {
        const { formFields, dateFields } = this.props
        formFields[object.id].value = value
        dateFields[object.id] = this._picker.getDate().toString()
        this.validate()
    }

    onValueChange = (object, value) => {
        const { formFields } = this.props
        console.log(value)
        formFields[object.id].value = value
        console.log(formFields[object.id].value)
        this.validate()
    }

    onFail = (error) => {
        const { formState } = this.props
        formState.enableSubmit = true
        formState.isSubmit = false
        this.validate()
    }

    onPress = () => {
        const { formState } = this.props
        formState.enableSubmit = false
        formState.isSubmit = true
        this.props.onPressMainButton(this.onFail)
    }


    _renderFields = (object) => {
        let { formFields } = this.props
        if (object.requires) {
            if (!formFields[object.requires].value) return <View key={object.id} />
        }

        let error = formFields[object.id].errorMessage != ''
        let success = formFields[object.id].errorMessage == '' && formFields[object.id].value != '' && object.register
        let type;
        if (error) type = 'error'
        else if (success) type = 'success'

        if (object.type == 'text') {
            return (
                <TextInput
                    key={object.id}
                    label={object.label}
                    placeholder={object.placeholder}
                    value={formFields[object.id].value}
                    keyboardType={object.keyboardType}
                    secureTextEntry={object.secure}
                    mask={object.mask}
                    maxLength={object.maxLength}
                    type={type || 'default'}
                    message={formFields[object.id].errorMessage}
                    prefix={object.prefix}
                    suffix={object.suffix}
                    onChangeText={(value) => this.onChangeText(object, value)}
                    onBlur={() => this.onBlur()}
                    disabled={false}
                    {...object.props}
                />
            )
        }
        else if (object.type == 'datePicker') {
            return (
                <DatePicker
                    ref={picker => this._picker = picker}
                    key={object.id}
                    type={error ? 'error' : 'default'}
                    date={formFields[object.id].value}
                    mode={'date'}
                    message={formFields[object.id].errorMessage}
                    placeholder={DatePlaceholder}
                    onDateChange={(value) => this.onDateChange(object, value)}
                    disabled={false}
                    {...object.props}
                />
            )
        }
        else if (object.type == 'switch') {
            return (
                <Switch
                    key={object.id}
                    label={object.label}
                    value={formFields[object.id].value}
                    onValueChange={(value) => this.onValueChange(object, value)}
                    disabled={false}
                    type={'default'}
                    message={''}
                    {...object.props}
                />
            )
        }
        else if (object.type == 'picker') {
            return (
                <Picker
                    key={object.id}
                    value={formFields[object.id].value}
                    onValueChange={(value) => this.onValueChange(object, value)}
                    items={object.items}
                    {...object.props}
                />
            )
        }
        else {
            return <View key={object.id} />
        }
    }

    render() {
        const {
            informationList,
            buttonLabel,
            formState,
            enableQuickLink,
            onPressQuickLink,
            quickLinkRight,
            quickLinkLeft,
            quickLinkLabel,
            enableExtraButton,
            onPressExtraButton,
            extraButtonProps,
            extraButtonLabel
        } = this.props

        const MainButton = (props) => (
            <Button
                full
                rounded
                success
                disabled={!props.enableSubmit}
                onPress={() => this.onPress()}
            >
                {!props.isSubmit
                    ? <Text>{buttonLabel}</Text>
                    : <ActivityIndicator color={Colors.background} size={"large"} />  
                }
            </Button>
        )

        const QuickLink = () => (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}
            >
                <TouchableWithoutFeedback
                    onPress={onPressQuickLink}
                >
                    <Text
                        style={[
                            styles.textButtonStyle,
                            { 
                                marginLeft: quickLinkRight ? 'auto' : '4%',
                                marginRight: quickLinkLeft ? 'auto': '4%', 
                            }
                        ]}
                    >
                        {quickLinkLabel}
                    </Text>
                </TouchableWithoutFeedback>
            </View>
        )

        return (
            <KeyboardAvoidingView
                behavior={
                    Platform.OS === 'ios' 
                        ? 'padding'
                        : undefined
                }
                style={{ flex: 1, backgroundColor: Colors.background }}
            >
                <ScrollView
                    style={{ flex: 1 }}
                    keyboardShouldPersistTaps={'handled'}
                    contentContainerStyle={[ 
                        styles.container,
                        this.props.contentContainerStyle
                    ]}
                >
                    <Card>
                        {informationList.map(object => this._renderFields(object))}
                        {enableExtraButton && enableQuickLink &&
                            <QuickLink />       
                        }
                        {enableExtraButton &&
                            <MainButton isSubmit={formState.isSubmit} enableSubmit={formState.enableSubmit} />
                        }
                    </Card>
                    {!enableExtraButton &&
                        <Card>
                            {enableQuickLink &&
                                <QuickLink />
                            }
                                <MainButton isSubmit={formState.isSubmit} enableSubmit={formState.enableSubmit} />
                        </Card>
                    }
                    {enableExtraButton &&
                        <Card>
                            <Button
                                full
                                rounded
                                onPress={onPressExtraButton}
                                {...extraButtonProps}
                            >
                                    <Text>{extraButtonLabel}</Text>
                            </Button>
                        </Card>
                    }
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1, 
        justifyContent: 'space-between',
    },  
    formView: {
        flexDirection: "column",
        padding: 10,
    },
    accordMainLabel: {
        fontWeight: 'bold',
        marginLeft: 15
    },
    accordSecondLabel: {
        fontSize: 12,
        marginLeft: 'auto',
        marginRight: '5%'
    },
    accordContent: {
        padding: 10,
    },
    errorMessage: {
        color: Colors.redDeny,
        marginLeft: 25,
        marginBottom: 2,
        fontSize: 13
    },
    cardItemStyle: {
        marginTop: '5%',
        marginBottom: '5%',
        alignItems: 'center'
    },
    textButtonStyle: {
        color: Colors.link,
        borderBottomColor: Colors.link,
        borderBottomWidth: 0.2,
        marginBottom: '4%',
        fontSize: 13
    },
    pickerStyle: {
        width: '100%'
    }
})