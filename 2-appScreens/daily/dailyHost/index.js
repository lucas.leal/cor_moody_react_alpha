import React, { Component } from 'react'
import { toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import { 
    DailyStrings as strings,
} from '@cor/config'
import { CarouselListScreen } from '@cor/screens/shared'
import { MemberListCard } from '@cor/components'

@inject('appStore')
@observer
export default class Daily extends Component {

    static title = strings.headerForm

    state = {
        statsCache: []
    }

    componentWillMount = () => {
        const { appStore } = this.props

        appStore.resetForm()
        if (appStore.focusedDaily.status == 'ongoing') {
            let defaultDailyStats = {
                pto: false,
                mood: 'happy',
                presence: 'time',
                board: 'updated',
                video: 'on'
            }
    
            appStore.generateDailyForm(defaultDailyStats)
        }
        else {
            appStore.getDailyForm()
        }
    }

    componentWillUnmount = () => {
        const { appStore } = this.props
        appStore.saveDaily()
        appStore.saveProjects()
    }

    generateButtonsData = () => [
        {
            id: 'presence',
            type: 'select-one',
            label: strings.presence,
            onPress: this.onSelectButton,
            selected: this.isButtonSelected,
            disabled: (userId) => this.props.appStore.dailyFields[userId].pto,
            buttons: [
                { id: 'time',   name: 'account' },
                { id: 'late',   name: 'clock' },
                { id: 'absent', name: 'account-remove' }
            ]
        },
        {
            id: 'mood',
            type: 'select-one',
            label: strings.mood,
            onPress: this.onSelectButton,
            selected: this.isButtonSelected,
            disabled: this.isDisabled,
            buttons: [
                { id: 'happy', name: 'emoticon-excited' },
                { id: 'ok',    name: 'emoticon-neutral' },
                { id: 'sad',   name: 'emoticon-sad' }
            ]
        },
        {
            id: 'misc',
            type: 'select-multiple',
            disabled: this.isDisabled,
            buttons: [
                { 
                    id: 'board', 
                    name: 'trello',
                    activeStat: 'updated',
                    deactStat: 'notupdated',
                    label: strings.board, 
                    onPress: this.onPressSingleButton,
                    selected: this.isSingleButtonSelected,
                },
                { 
                    id: 'video', 
                    name: 'video',
                    activeStat: 'on',
                    deactStat: 'off',
                    label: strings.video, 
                    onPress: this.onPressSingleButton,
                    selected: this.isSingleButtonSelected,
                }
            ]
        },
    ]

    setPTO = (userId) => {
        const { dailyFields } = this.props.appStore
        let statsCache = this.state.statsCache
        if (!dailyFields[userId].pto) {
            statsCache[userId] = toJS(dailyFields[userId])
            this.setState({ statsCache: statsCache}, () => {
                dailyFields[userId].pto = true
                dailyFields[userId].presence = 'pto'
                dailyFields[userId].mood = 'pto'
                dailyFields[userId].board = 'pto'
                dailyFields[userId].video = 'pto'
            })
        }
        else if (this.state.statsCache[userId]) {
            dailyFields[userId].pto = false
            dailyFields[userId].presence = this.state.statsCache[userId].presence
            dailyFields[userId].mood = this.state.statsCache[userId].mood
            dailyFields[userId].board = this.state.statsCache[userId].board
            dailyFields[userId].video = this.state.statsCache[userId].video
            statsCache[userId] = undefined
            this.setState({ statsCache: statsCache })
        }
    }

    isDisabled = (userId) => {
        const { dailyFields } = this.props.appStore
        return dailyFields[userId].presence == 'absent' || dailyFields[userId].pto
    }

    shouldSetAllAbsent = (isAbsent, userId) => {
        //debugger
        const { dailyFields } = this.props.appStore
        let statsCache = this.state.statsCache
        if (isAbsent) {
            statsCache[userId] = toJS(dailyFields[userId])
            this.setState({ statsCache: statsCache}, () => {
                dailyFields[userId].mood = 'absent'
                dailyFields[userId].board = 'absent'
                dailyFields[userId].video = 'absent'
            })
        }
        else if (this.state.statsCache[userId]) {
            dailyFields[userId].mood = this.state.statsCache[userId].mood
            dailyFields[userId].board = this.state.statsCache[userId].board
            dailyFields[userId].video = this.state.statsCache[userId].video
            statsCache[userId] = undefined
            this.setState({ statsCache: statsCache })
        }
    }

    onSelectButton = (rowId, buttonId, userId) => {
        const { dailyFields } = this.props.appStore
        dailyFields[userId][rowId] = buttonId
        this.shouldSetAllAbsent(buttonId == 'absent', userId)
    }

    isButtonSelected = (rowId, buttonId, userId) => {
        const { appStore } = this.props
        return appStore.dailyFields[userId][rowId] == buttonId
    }

    onPressSingleButton = (buttonId, userId, activeStat, deactStat) => {
        const { dailyFields } = this.props.appStore
        if (dailyFields[userId][buttonId] == activeStat) dailyFields[userId][buttonId] = deactStat
        else dailyFields[userId][buttonId] = activeStat
        //this.shouldSetAllAbsent(dailyFields[userId]['presence'] == 'absent', userId)
    }

    isSingleButtonSelected = (buttonId, userId, activeStat, deactStat) => {
        const { appStore } = this.props
        return appStore.dailyFields[userId][buttonId] == activeStat
    }

    render() {
        const { appStore } = this.props
        const functions = {
            setPTO: this.setPTO
        }
        return (
            <CarouselListScreen
                RenderComponent={MemberListCard}
                data={appStore.dailyFields}
                buttonsData={this.generateButtonsData()}
                functions={functions}
                pagination
            />
        )
    }
}
