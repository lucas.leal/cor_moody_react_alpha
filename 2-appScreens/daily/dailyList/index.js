import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { ListScreen } from '@cor/screens/shared'
import { DailyListItem } from '@cor/components'
import { DailyStrings as strings } from '@cor/config'

@inject('appStore')
@inject('userStore')
@observer
export default class DailyList extends Component {

    static title = strings.header

    reopenDaily = (item) => {
        const {
            appStore
        } = this.props

        appStore.reopenDaily(item.id)
        appStore.saveProjects()
    }

    cancelDaily = (item) => {
        const {
            appStore
        } = this.props

        appStore.cancelDaily(item.id)
        appStore.saveProjects()
    }

    onListPress = (item) => {
        const {
            navigation,
            appStore
        } = this.props
        appStore.iFocusedDaily = item.id
        const args = [`${appStore.focusedDaily.id+1}`]
        navigation.navigate('Daily', { args: args })
    }

    render() {
        const swipeoutFunctions = {
            cancelDaily: this.cancelDaily,
            reopenDaily: this.reopenDaily
        }

        return (
            <ListScreen
                data={this.props.appStore.focusedSprintDailies}
                RenderComponent={DailyListItem}
                onListPress={this.onListPress}
                handleBackPress={() => this.props.navigation.goBack()}
                swipeoutFunctions={swipeoutFunctions}
            />
        );
    }
}
