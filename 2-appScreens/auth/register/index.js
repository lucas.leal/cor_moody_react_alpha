import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import { Constants, UserStrings as strings } from '@cor/config'
import { FormScreen } from '@cor/screens/shared'

@inject('userStore')
@observer
export default class RegisterPart1 extends Component {

    static title = strings.registerHeader

    informationList = [
        {
            id: strings.email.id,
            value: Constants.isDebug ? Constants.DebugInfo.account : undefined,
            label: strings.email.label,
            keyboardType: strings.email.type,
            maxLength: 60,
            mask: '',
            secure: false,
            register: true,
            type: 'text',
            props: {
                autoCapitalize: 'none',
                //autoComplete: 'off',
                autoCorrect: false
            }
        },
        {
            id: strings.password.id,
            value: Constants.isDebug ? Constants.DebugInfo.password : undefined,
            label: strings.password.label,
            keyboardType: strings.password.type,
            maxLength: 40,
            mask: '',
            secure: true,
            register: true,
            type: 'text'
        },
        {
            id: strings.confirmPassword.id,
            value: Constants.isDebug ? Constants.DebugInfo.password : undefined,
            label: strings.confirmPassword.label,
            keyboardType: strings.confirmPassword.type,
            maxLength: 40,
            mask: '',
            secure: true,
            register: true,
            type: 'text'
        }
    ]

    onSuccess = () => {
        const { userStore, navigation } = this.props
        userStore.formState.enableSubmit = false        
        userStore.formState.isSubmit = false
        userStore.resetForm()
        navigation.navigate('ProjectList')
    }
    
    onPress = (onFail) => {
        this.props.userStore.register(this.onSuccess, onFail)
    }

    render() {
        return (
            <Container>
                <FormScreen
                    formFields={this.props.userStore.userFields}
                    informationList={this.informationList}
                    onPressMainButton={this.onPress}
                    buttonLabel={strings.registerButton.label}
                    formState={this.props.userStore.formState}
                />
            </Container>
        );
    }
}
