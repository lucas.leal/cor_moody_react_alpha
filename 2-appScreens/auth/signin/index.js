import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Header } from '@cor/components'
import { Button, Container, Tab, Tabs, Content } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Constants, UserStrings as strings } from '@cor/config'
import { FormScreen } from '@cor/screens/shared'

@inject('userStore')
@observer
export default class SignIn extends Component {
    informationList = [
        {
            id: strings.email.id,
            value: Constants.isDebug ? Constants.DebugInfo.account : undefined,
            //label: strings.email.label,
            keyboardType: strings.email.type,
            maxLength: 60,
            mask: '',
            secure: false,
            register: false,
            type: 'text',
            prefix: (color) => <Icon name='email-outline' size={19} color={color} />,
            highlightPrefix: true,
            props: {
                autoCapitalize: 'none',
                //autoComplete: 'off',
                autoCorrect: false
            }
        },
        {
            id: strings.password.id,
            value: Constants.isDebug ? Constants.DebugInfo.password : undefined,
            //label: strings.password.label,
            keyboardType: strings.password.type,
            maxLength: 40,
            mask: '',
            secure: true,
            register: false,
            type: 'text',
            prefix: (color) => <Icon name='lock' size={19} color={color} />,
            highlightPrefix: true,
        }
    ]

    onSuccess = () => {
        const { userStore, navigation } = this.props
        userStore.formState.enableSubmit = false        
        userStore.formState.isSubmit = false
        userStore.resetForm()        
        navigation.navigate('ProjectList')
    }

    onPress = (onFail) => {
        this.props.userStore.login(this.onSuccess, onFail)
    }

    render() {
        return (
            <FormScreen
                formFields={this.props.userStore.userFields}
                informationList={this.informationList}
                onPressMainButton={this.onPress}
                buttonLabel={strings.signinButton.label}
                formState={this.props.userStore.formState}
                enableExtraButton
                extraButtonLabel={strings.dontHaveAccount}
                onPressExtraButton={() => this.props.navigation.navigate('Register')}
            />
        )
    }
}
