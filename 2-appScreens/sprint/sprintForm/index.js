import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { SprintStrings as strings } from '@cor/config'
import { FormScreen } from '@cor/screens/shared'

@inject('appStore')
@inject('userStore')
@observer
export default class SprintForm extends Component {

    static title = strings.headerForm
    
    today = new Date()
    informationList = [
        {
            id: strings.sprintDate.id,
            type: 'datePicker',
            showDay: false,
            today: this.today,
            props: {
                label: strings.sprintDate.label,
                format: 'D/M/YYYY',
                minDate: this.past,
                maxDate: this.future,
                register: false,
                androidMode: 'default'
            }
        },
        {
            value: this.props.appStore.sprintFields.numberOfDailies.value || '',
            id: strings.numberOfDailies.id,
            type: 'picker',
            props: {
                label: strings.numberOfDailies.label
            },
            items: [
                {label: '5', value: 5},
                {label: '10', value: 10},
                {label: '15', value: 15},
            ]
        },
    ]

    onSuccess = () => {
        const { appStore, navigation } = this.props
        appStore.resetFormState()
        appStore.resetForm()        
        navigation.navigate('SprintList')
    }

    onPress = (onFail) => {
        this.props.appStore.addSprint()
        this.props.appStore.saveProjects(this.onSuccess, onFail)
    }

    render() {
        return (
            <Container>
                <FormScreen
                    formFields={this.props.appStore.sprintFields}
                    dateFields={this.props.appStore.sprintDates}
                    informationList={this.informationList}
                    onPressMainButton={this.onPress}
                    buttonLabel={strings.saveButton.label}
                    formState={this.props.appStore.formState}
                />
            </Container>
        );
    }
}
