import React, { Component } from 'react'
import { Platform } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import { ListScreen } from '@cor/screens/shared'
import { SprintListItem } from '@cor/components'
import { 
    Colors, 
    Constants ,
    SprintStrings as strings
} from '@cor/config'
import { renderHeaderButton } from '@cor/config/helpers'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

@inject('appStore')
@inject('userStore')
@observer
export default class SprintList extends Component {

    /**
     * Header
     */

    static title = strings.header

    /**
     * Component Start
     */

    baseSwipeoutProps = {
        left: [
            {
                text: <Icon name={'delete'} size={30} color={Colors.text} />,
                callback: item => this.deleteItem(item),
                backgroundColor: Colors.redDeny
            }
        ]
    }

    fabActions = [
        {
            text: strings.newSprintButton.label,
            color: Colors.primary,
            icon: <Icon name={'plus'} size={10} color={Colors.white} />,
            name: strings.newSprintButton.id,
            position: 1
        }
    ]

    deleteItem = (item) => {
        const {
            appStore
        } = this.props

        appStore.removeSprint(item.id)
        appStore.saveProjects()
    }

    onListPress = (item) => {
        const {
            navigation,
            appStore
        } = this.props
        appStore.iFocusedSprint = item.id
        const args = [`${appStore.focusedSprint.id+1}`]
        navigation.navigate('DailyList', { args: args })
    }

    onPressItem = (id) => {
        const { navigation } = this.props

        switch (id) {
            case strings.newSprintButton.id:
                navigation.navigate('SprintForm')
                break
            default:
                console.log('Error')
        }
    }

    render() {
        const floatingActionProps = {
            actions: this.fabActions,
            onPressItem: this.onPressItem,
            color: Colors.primary
        }

        const swipeoutFunctions = {
            deleteItem: this.deleteItem
        }

        return (
            <ListScreen
                data={this.props.appStore.focusedReleaseSprints}
                RenderComponent={SprintListItem}
                showFab
                floatingActionProps={floatingActionProps}
                onListPress={this.onListPress}
                swipeoutFunctions={swipeoutFunctions}
            />
        );
    }
}
