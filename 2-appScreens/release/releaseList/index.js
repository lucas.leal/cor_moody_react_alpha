import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Colors, ReleaseStrings as strings } from '@cor/config'
import { ListScreen } from '@cor/screens/shared'
import { ReleaseListItem } from '@cor/components'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

@inject('appStore')
@inject('userStore')
@observer
export default class ReleaseScreen extends Component {
    
    static title = strings.header

    fabActions = [
        {
            text: strings.manageMembersButton.label,
            color: Colors.primary,
            icon: <Icon name={'account-plus'} size={10} color={Colors.white} />,
            name: strings.manageMembersButton.id,
            position: 2
        },
        {
            text: strings.newReleaseButton.label,
            color: Colors.primary,
            icon: <Icon name={'plus'} size={10} color={Colors.text} />,
            name: strings.newReleaseButton.id,
            position: 1
        }
    ]

    baseSwipeoutProps = {
        left: [
            {
                text: <Icon name={'delete'} size={30} color={Colors.text} />,
                callback: item => this.deleteItem(item),
                backgroundColor: Colors.redDeny
            }
        ]
    }

    deleteItem = (item) => {
        const {
            appStore
        } = this.props

        appStore.removeRelease(item.id)
        appStore.saveProjects()
    }

    onListPress = (item) => {
        const {
            navigation,
            appStore
        } = this.props
        appStore.iFocusedRelease = item.id
        const args = [`${appStore.focusedRelease.id+1}`]
        navigation.navigate('SprintList', { args: args })
    }

    onPressItem = (id) => {
        const { navigation } = this.props
        switch (id) {
            case strings.manageMembersButton.id:
                navigation.navigate('MembersList')
                break
            case strings.newReleaseButton.id:
                navigation.navigate('ReleaseForm')
                break
            default:
                console.log('Error')
        }
    }

    render() {
        const floatingActionProps = {
            actions: this.fabActions,
            onPressItem: this.onPressItem,
            color: Colors.primary
        }

        const swipeoutFunctions = {
            deleteItem: this.deleteItem
        }
         
        return (
            <ListScreen
                ref={list => this._list = list}
                data={this.props.appStore.focusedProjectReleases}
                RenderComponent={ReleaseListItem}
                showFab
                floatingActionProps={floatingActionProps}
                onListPress={this.onListPress}
                swipeoutFunctions={swipeoutFunctions}
            />
        )
    }
}
