import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { 
    Colors,
    ReleaseStrings as strings
} from '@cor/config'
import { FormScreen } from '@cor/screens/shared'
import { createLaterDate } from '@cor/config/helpers'

@inject('appStore')
@inject('userStore')
@observer
export default class ReleaseForm extends Component {

    static title = strings.headerForm

    today = new Date()
    past = createLaterDate(this.today, -4)
    future = createLaterDate(this.today, 4)
    informationList = [
        {
            id: strings.releaseDate.id,
            type: 'datePicker',
            showDay: false,
            today: this.today,
            props: {
                label: strings.releaseDate.label,
                format: 'M/YYYY',
                minDate: this.past,
                maxDate: this.future,
                register: false,
            }
        }
    ]

    onSuccess = () => {
        const { appStore, navigation } = this.props
        appStore.resetFormState()
        appStore.resetForm()        
        navigation.navigate('ReleaseList')
    }

    onPress = (onFail) => {
        this.props.appStore.addRelease()
        this.props.appStore.saveProjects(this.onSuccess, onFail)
    }
    
    render() {
        const {
            navigation
        } = this.props

        return (
            <FormScreen
                formFields={this.props.appStore.releaseFields}
                dateFields={this.props.appStore.releaseDates}
                informationList={this.informationList}
                onPressMainButton={this.onPress}
                buttonLabel={strings.saveButton.label}
                formState={this.props.appStore.formState}
            />
        )
    }
}
