import React, { Component } from 'react'
import { Platform } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import { ListScreen } from '@cor/screens/shared'
import { ProjectListItem } from '@cor/components'
import { 
    Colors, 
    Constants, 
    ProjectStrings as strings
} from '@cor/config'
import { renderHeaderButton } from '@cor/config/helpers'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

@inject('appStore')
@inject('userStore')
@observer
export default class ProjectList extends Component {

    static title = strings.header

    /**
     * Component start
     */
    
    fabActions = [
        {
            text: strings.generateReportButton.label,
            color: Colors.primary,
            icon: <Icon name={'share-variant'} size={10} color={Colors.white} />,
            name: strings.generateReportButton.id,
            position: 2
        },
        {
            text: strings.newProjectButton.label,
            color: Colors.primary,
            icon: <Icon name={'plus'} size={10} color={Colors.white} />,
            name: strings.newProjectButton.id,
            position: 1
        }
    ]
    
    componentWillMount = () => {
        this.props.appStore.loadProjects() 
    }

    editItem = (item) => {
        const {
            appStore,
            navigation,
        } = this.props

        appStore.projectFields.name.value = appStore.projects[item.id].name
        appStore.projectFields.desc.value = appStore.projects[item.id].desc

        const params = { 
            isEditing: true, 
            editIndex: item.id,
        }

        navigation.navigate('ProjectForm', params)
    }

    deleteItem = (item) => {
        const {
            appStore
        } = this.props

        appStore.removeProject(item.id)
        appStore.saveProjects()
    }

    generateReport = () => {
        const { appStore, userStore } = this.props

        const today = new Date()
        const strToday = `${today.getFullYear()}/${today.getMonth()+1}/${today.getDate()}`

        const title = strings.shareTitle.replace('{0}', userStore.user.email)
        const message = strings.shareMessage.replace('{0}', strToday)
        this.props.appStore.generateProjectsReport(title, message, title)
    }

    onListPress = (item) => {
        const {
            navigation,
            appStore
        } = this.props

        appStore.iFocusedProject = item.id
        const args = [appStore.focusedProject.name]
        navigation.navigate('ReleaseList', { args: args })
    }

    onPressItem = (id) => {
        switch (id) {
            case strings.generateReportButton.id:
                this.generateReport()
                break
            case strings.newProjectButton.id:
                this.props.navigation.navigate('ProjectForm')
                break
            default:
                console.log('Error')
        }
    }

    render() {
        const floatingActionProps = {
            actions: this.fabActions,
            onPressItem: this.onPressItem,
            color: Colors.primary
        }

        const swipeoutFunctions = {
            deleteItem: this.deleteItem,
            editItem: this.editItem
        }

        return (
            <ListScreen
                ref={list => this._list = list}
                data={this.props.appStore.projects}
                RenderComponent={ProjectListItem}
                showFab
                floatingActionProps={floatingActionProps}
                onListPress={this.onListPress}
                swipeoutFunctions={swipeoutFunctions}
            />
        )
    }
}
