import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Container } from 'native-base'
import { FormListScreen } from '@cor/screens/shared'
import { MemberListItem } from '@cor/components'
import { ProjectStrings as strings  } from '@cor/config'

@inject('appStore')
@inject('userStore')
@observer
export default class MembersList extends Component {

    static title = strings.headerMembers
    
    _newItem = (id) => {
        return {
            id: id,
            value: '',
            staticProps: {
                score: 0
            }
        }   
    }

    componentWillMount = () => {
        const {
            appStore
        } = this.props

        /**
         * Creating a generic informationList for listForm, 
         * since listForm is a Shared component it shouldn't be specific
         */
        appStore.loadMembersList()
    }

    componentWillUnmount = () => {
        const {
            appStore
        } = this.props

        appStore.saveMembersList()
        appStore.saveProjects()
    }

    render() {
        return (
            <Container>
                <FormListScreen 
                    RenderComponent={MemberListItem}
                    data={this.props.appStore.membersFields}
                    newValueString={strings.newMember}
                    newItemObject={this._newItem}
                />
            </Container>
        )
    }
}
