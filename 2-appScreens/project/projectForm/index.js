import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { 
    ProjectStrings as strings
} from '@cor/config'
import { FormScreen } from '@cor/screens/shared'

@inject('appStore')
@inject('userStore')
@observer
export default class ProjectForm extends Component {

    static title = strings.headerForm
    
    informationList = [
        {
            value: this.props.appStore.projectFields.name.value || '',
            id: strings.name.id,
            label: strings.name.label,
            keyboardType: strings.name.type,
            maxLength: 10,
            mask: '',
            secure: false,
            register: false,
            type: 'text'
        },
        {
            value: this.props.appStore.projectFields.desc.value || '',
            id: strings.desc.id,
            label: strings.desc.label,
            keyboardType: strings.desc.type,
            maxLength: 30,
            mask: '',
            secure: false,
            register: false,
            type: 'text',
            allowEmpty: true
        }
    ]

    onSuccess = () => {
        const { appStore, navigation } = this.props
        appStore.resetFormState()
        appStore.resetForm()        
        navigation.navigate('ProjectList')
    }

    onPress = (onFail) => {
        this.props.appStore.addProject()
        this.props.appStore.saveProjects(this.onSuccess, onFail)
    }

    onPressEdit = (onFail) => {
        const {
            appStore,
            navigation
        } = this.props

        const editIndex = navigation.getParam('editIndex', -1)

        appStore.editProject(editIndex)
        appStore.saveProjects(this.onSuccess, onFail)
    }

    render() {
        const {
            navigation
        } = this.props

        const isEditing = navigation.getParam('isEditing', false)
     
        return (
            <FormScreen
                formFields={this.props.appStore.projectFields}
                informationList={this.informationList}
                onPressMainButton={!isEditing 
                    ? this.onPress
                    : this.onPressEdit
                }
                buttonLabel={strings.saveButton.label}
                formState={this.props.appStore.formState}
            />
        );
    }
}
