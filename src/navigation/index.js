import React, { Component } from "react"
import { View } from 'react-native'
import { createStackNavigator } from "react-navigation"
import { Colors } from '@cor/config'
import { baseOpts } from '@cor/config/helpers'
import FlashMessage from "react-native-flash-message"
import { 
  Daily,
  DailyList,
  ProjectList,
  ProjectForm,
  SprintList,
  SprintForm,
  SignIn,
  Register,
  MembersList,
  ReleaseList,
  ReleaseForm
} from '@cor/screens'

export default class Navigation extends Component {
    render() {
        Navigation = createStackNavigator({
            SignIn: { 
                screen: SignIn,
                navigationOptions: opt => baseOpts(opt.navigation, '', true, true)
            },
            Register: { 
                screen: Register,
                navigationOptions: opt => baseOpts(opt.navigation, Register.title)
            },
            ProjectList: { 
                screen: ProjectList,
                navigationOptions: opt => baseOpts(opt.navigation, ProjectList.title, true)
            },
            ProjectForm: { 
                screen: ProjectForm,
                navigationOptions: opt => baseOpts(opt.navigation, ProjectForm.title)
            },
            ReleaseList: {
                screen: ReleaseList,
                navigationOptions: opt => baseOpts(opt.navigation, ReleaseList.title)
            },
            ReleaseForm: {
                screen: ReleaseForm,
                navigationOptions: opt => baseOpts(opt.navigation, ReleaseForm.title)
            },
            SprintList: { 
                screen: SprintList,
                navigationOptions: opt => baseOpts(opt.navigation, SprintList.title)
            },
            SprintForm: { 
                screen: SprintForm,
                navigationOptions: opt => baseOpts(opt.navigation, SprintForm.title)
            },
            MembersList : {
                screen: MembersList,
                navigationOptions: opt => baseOpts(opt.navigation, MembersList.title)
            },
            DailyList: { 
                screen: DailyList,
                navigationOptions: opt => baseOpts(opt.navigation, DailyList.title)
            },
            Daily: { 
                screen: Daily,
                navigationOptions: opt => baseOpts(opt.navigation, Daily.title)
            },
          }
        )

        return (
            <View style={{flex: 1}}>
                <Navigation />
                <FlashMessage position='top' />
            </View>
        )
    }
}