import React, { Component } from 'react'
import { View } from 'react-native'
import { Provider, observer } from 'mobx-react'
import firebase from 'firebase'
import Navigation from './navigation'
import Stores from './mobx'
import FlashMessage from "react-native-flash-message"
import getTheme from '@cor/config/themes/components'
import { Constants } from '@cor/config'
import moody from '@cor/config/themes/variables/moody'
import { StyleProvider } from 'native-base'

@observer
export default class App extends Component {
    componentWillMount() {
        var config = Constants.Environments[Constants.activeEnvironment]
        firebase.initializeApp(config);
        console.disableYellowBox = true
    }

    render() {
        return (
            <Provider
                appStore={Stores.appStore}
                userStore={Stores.userStore}
            >
                <StyleProvider style={getTheme(moody)}>
                    <Navigation />
                </StyleProvider>
            </Provider>
        )
    }
}

//
