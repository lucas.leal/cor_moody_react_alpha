import { observable, action, computed } from 'mobx'
import { UserStrings as strings } from '@cor/config'
import firebase from 'firebase'
import { showMessage } from 'react-native-flash-message';

export default class UserStore {
    @observable user = {
        uid: '',
        email: '',
    }

    @observable userFields = {
        email: { 
            value: '', 
            message: '', 
            validate: (email) => this.validateEmail(email) 
        },
        password: { 
            value: '', 
            message: '', 
            validate: (password) => this.validatePassword(password) 
        },
        confirmPassword: { 
            value: '', 
            message: '', 
            validate: (password, confirm) => this.validateConfirmPassword(password, confirm) 
        }
    }

    @observable formState = {
        enableSubmit: false,
        isSubmit: false,
    }

    @action
    resetForm = () => {
        this.userFields.email.value = ''
        this.userFields.password.value = ''
        this.userFields.confirmPassword.value = ''
    }

    @action
    validateEmail = () => {
        email = this.userFields.email.value

        const {  
            invalid,
            short,
            obligatory,
            //error,
        } = strings.email.errorMessage

        let pattern = /^(([^<>()\[\]\\.,;:\s@çÇ"]+(\.[^<>()\[\]\\.,;:\s@çÇ"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (email.length == 0) return obligatory
        else if (email.length < 5) return short
        else if (!pattern.test(email)) return invalid
        else return ''
    }

    @action
    validatePassword = () => {
        password = this.userFields.password.value

        const {  
            requires,
            requiresTwo,
            or,
            //and,
            invalidNumber,
            //invalidLower,
            invalidUpper,
            invalidSpecial,
            short,
            obligatory,
            //obligatoryNumber,
            obligatoryLower,
            //obligatoryUpper,
            //obligatorySpecial,
            //error,
        } = strings.password.errorMessage
        
        let patLength = /(?=.{8,})/g
        let patLower = /^(?=.*[a-z])/g

        /* Obligatory section */
        if (password.length == 0) return obligatory
        else if (!patLower.test(password)) return obligatoryLower
        else if (!patLength.test(password)) return short

        /* Choice section */
        let validations = [
            { 
                pattern: /^(?=.*[A-Z])/g,
                message: invalidUpper
            },
            { 
                pattern: /^(?=.*[0-9])/g, 
                message: invalidNumber
            },
            { 
                pattern: /^(?=.*[!@#\$%\^&\*])/g, 
                message: invalidSpecial
            },
        ]

        let cont = 0
        let endMsg = ''
        for (let i = 0; i < validations.length; i++) {
            if (!validations[i].pattern.test(password)) {
                let sep = cont > 0 
                    ? or + ' ' 
                    : ''
                endMsg += ' ' + sep + validations[i].message

                cont++
            }

        }

        if (cont > 1 ) {
            let startMsg = cont > 2
                ? requiresTwo
                : requires

            return startMsg + '' + endMsg
        }
        
        return ''
    }

    @action
    validateConfirmPassword = () => {
        password = this.userFields.password.value
        confirmPassword = this.userFields.confirmPassword.value

        const { 
            obligatory,
            obligatoryMain,
            invalid,
            invalidMain,
            //error,
        } = strings.confirmPassword.errorMessage

        if (confirmPassword.length == 0) return obligatory
        else if (password.length == 0) return obligatoryMain
        else if (this.validatePassword() != '') return invalidMain
        else if (password !== confirmPassword) return invalid
        else return ''
    }

    @action
    login = async (onSuccess, onFail) => {
        let userFields = this.userFields
        let email = userFields.email.value
        let password = userFields.password.value

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then( authData => {
            firebase.database().ref(`users/${authData.user.uid}`).once('value')
            .then( snapshot => {
                let updatedUser = snapshot.val().account
                this.user = updatedUser
            })
            onSuccess()
        })
        .catch( error => {
            /* Treat any exceptions here */
            showMessage({
                type: 'danger',
                message: strings.firebaseAuth[error.code].code,
                description: strings.firebaseAuth[error.code].message,
            })
            onFail(error)
        })
    }

    @action
    register = async (onSuccess, onFail) => {
        let userFields = this.userFields
        let email = userFields.email.value
        let password = userFields.password.value
        /* Create user with email and password */
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then( authData => {
            /* Afterwards create an object to be added to the database */
            let account = {
                uid: authData.user.uid,
                email: email
            }
            /* Add to path users/uid for uniqueness */
            firebase.database().ref(`users/${authData.user.uid}`).set({account})
            .then(() => {
                /* Update MOBX storage with info sent to Firebase */
                firebase.database().ref(`users/${authData.user.uid}`).once('value')
                .then( snapshot => {
                    let updatedUser = snapshot.val().account
                    this.user = updatedUser
                })
            })
            onSuccess()
        })
        .catch( error => {
            /* Treat any exceptions here */
            showMessage({
                type: 'danger',
                message: strings.firebaseAuth[error.code].code,
                description: strings.firebaseAuth[error.code].message,
            })
            onFail(error)
        })
    }

    @action
    updateProfile = async () => {

    }
}