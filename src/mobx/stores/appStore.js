import {observable, action, computed, toJS} from 'mobx'
import { ProjectStrings as projStrings } from '@cor/config'
import { formatDate } from '@cor/config/helpers'
import { Platform } from 'react-native'
import firebase from 'firebase'
import Share from 'react-native-share'
import base64 from 'react-native-base64'

export default class AppStore {
    @observable projects = []

    /** 
     * Indexes 
     */
    @observable iFocusedProject = -1
    @observable iFocusedRelease = -1
    @observable iFocusedSprint = -1
    @observable iFocusedDaily = -1
    @observable iFocusedResult = -1


    /** 
     * Focuses 
     */
    @computed get focusedProject()         { return this.projects[this.iFocusedProject] }
    @computed get focusedRelease()         { return this.focusedProjectReleases[this.iFocusedRelease] }
    @computed get focusedSprint()          { return this.focusedReleaseSprints[this.iFocusedSprint] }
    @computed get focusedDaily()           { return this.focusedSprintDailies[this.iFocusedDaily] }
    @computed get focusedProjectMembers()  { if (!this.focusedProject.members) this.focusedProject.members = []; return this.focusedProject.members }
    @computed get focusedProjectReleases() { if (!this.focusedProject.releases) this.focusedProject.releases = []; return this.focusedProject.releases }
    @computed get focusedReleaseSprints()  { if (!this.focusedRelease.sprints) this.focusedRelease.sprints = []; return this.focusedRelease.sprints }
    @computed get focusedSprintDailies()   { if (!this.focusedSprint.dailies) this.focusedSprint.dailies = []; return this.focusedSprint.dailies }
    @computed get focusedDailyResults()    { if (!this.focusedDaily.results) this.focusedDaily.results = []; return this.focusedDaily.results }

    set focusedProject(value)         { this.projects[this.iFocusedProject] = value }
    set focusedRelease(value)         { this.focusedProjectReleases[this.iFocusedRelease] = value }
    set focusedSprint(value)          { this.focusedReleaseSprints[this.iFocusedSprint] = value }
    set focusedDaily(value)           { this.focusedSprintDailies[this.iFocusedDaily] = value }
    set focusedProjectMembers(value)  { this.focusedProject.members = value }
    set focusedProjectReleases(value) { this.focusedProject.releases = value }
    set focusedReleaseSprints(value)  { this.focusedRelease.sprints = value }
    set focusedSprintDailies(value)   { this.focusedSprint.dailies = value }
    set focusedDailyResults(value)    { this.focusedDaily.results = value }

    /**
     * New objects
     */
    newProject = (name, desc) => { return {
        id: this.projects.length || 0,
        name: name,
        desc: desc,
        members: [],
        releases: []
    }}
    newRelease = (date) => { return {
        id: this.focusedProjectReleases.length,
        date: date,
        status: 'ongoing',
        sprints: []
    }}
    newSprint = (numberOfDailies, date) => { return {
        id: this.focusedReleaseSprints.length,
        totalDays: numberOfDailies,
        completedDays: 0,
        status: 'ongoing',
        dailies: this.newDailyList(numberOfDailies, date)
    }}
    newDailyList = (numberOfDailies, date) => {
        console.log(date)
        let newDate = new Date(date)
        console.log(newDate)
        let dailies = []
        for (let i = 0; i < numberOfDailies; i++) {
            if (newDate.getDay() == 6) newDate.setDate(newDate.getDate()+2)
            dailies.push({
                id: i,
                date: newDate.toString(),
                status: 'ongoing',
                results: []
            })
            newDate.setDate(newDate.getDate()+1)
        }
        return dailies
    }


    /**
     * Form Configuration
     */
    @observable projectFields = {
        name: { 
            value: '', 
            message: '', 
            validate: () => this.validateProjectName()
        },
        desc: {
            value: '', 
            message: '', 
            validate: () => this.validateProjectDesc()
        },
    }
    @observable releaseFields = {
        releaseDate: {
            value: '', 
            message: '', 
            validate: () => ''
        }
    }
    @observable releaseDates = {
        releaseDate: ''
    }
    @observable sprintFields = {
        sprintDate: {
            value: '',
            message: '',
            validate: () => ''
        },
        numberOfDailies: { 
            value: '', 
            message: '', 
            validate: () => ''
        },
    }
    @observable sprintDates = {
        sprintDate: ''
    }
    @observable membersFields = []
    @observable dailyFields = []
    @observable formState = {
        enableSubmit: false,
        isSubmit: false,
    }


    /**
     * Actions
     */
    @action
    resetFormState = () => {
        this.formState.enableSubmit = false
        this.formState.isSubmit = false
    }

    @action
    resetForm = () => {
    }

    @action 
    validateProjectName = () => {
        let name = this.projectFields.name.value

        const {
            invalid,
            short,
            obligatory
        } = projStrings.name.errorMessage

        
        if (name.length == 0) return obligatory
        if (name.length < 3) return short
        let pattern = /^([a-zA-Z]){3,10}$/
        if (!pattern.test(name)) return invalid

        return ''
    }

    @action 
    validateProjectDesc = () => {
        let desc = this.projectFields.desc.value

        const {
            invalid,
            //short,
            //obligatory
        } = projStrings.desc.errorMessage

        if (desc.length > 0) {
            let pattern = /^([a-zA-Z ]){0,30}$/

            if (!pattern.test(desc)) return invalid
        }
        return ''
    }

    @action 
    addProject = () => {
        let currentProjects = this.projects || []
        let name = this.projectFields.name.value
        let desc = this.projectFields.desc.value
        currentProjects.push(this.newProject(name, desc))
        this.projects = currentProjects
    }

    @action 
    editProject = (index) => {
        let currentProjects = this.projects || []
        let name = this.projectFields.name.value
        let desc = this.projectFields.desc.value

        currentProjects[index].name = name
        currentProjects[index].desc = desc

        this.projects = currentProjects
    }

    @action 
    removeProject = (index) => {
        let currentProjects = this.projects || []
        currentProjects.splice(index, 1)
        for (let i = 0; i < currentProjects.length; i++) {
            currentProjects[i].id = i
        }

        this.projects = currentProjects
    }

    @action
    loadMembersList = () => {
        let membersList = this.focusedProjectMembers || []
            
        this.membersFields = []
        membersList.forEach(member => {
            this.membersFields.push({
                id: member.id,
                value: member.name,
                staticProps: {
                    score: member.score
                }
            })
        })
    }

    @action 
    saveMembersList = () => {
        let membersFields = this.membersFields

        this.focusedProjectMembers = []
        membersFields.forEach(field => {
            this.focusedProjectMembers.push({
                id: field.id,
                name: field.value,
                score: field.staticProps.score
            })
        })
    }

    @action 
    addRelease = () => {
        let date = this.releaseDates.releaseDate
        this.focusedProjectReleases.push(this.newRelease(date))
    }

    @action 
    removeRelease = (index) => {
        let newReleases = toJS(this.focusedProjectReleases) || []
        newReleases.splice(index, 1)
        for (let i = 0; i < newReleases.length; i++) newReleases[i].id = i
        this.focusedProjectReleases = newReleases
    }

    @action 
    addSprint = () => {
        let date = this.sprintDates.sprintDate
        let days = this.sprintFields.numberOfDailies.value
        this.focusedReleaseSprints.push(this.newSprint(days, date))
    }

    @action 
    removeSprint = (index) => {
        let newSprints = toJS(this.focusedReleaseSprints) || []
        newSprints.splice(index, 1)
        for (let i = 0; i < newSprints.length; i++) newSprints[i].id = i
        this.focusedReleaseSprints = newSprints
    }

    @action
    generateDailyForm = (defaultDailyStats) => {
        let newDailyForm = []
        this.focusedProjectMembers.forEach(member => {
            let dailyField = {
                ...member,
                ...defaultDailyStats
            }

            newDailyForm.push(dailyField)
        })

        this.dailyFields = newDailyForm
        console.log(this.dailyFields)
    }

    @action
    getDailyForm = () => {
        this.dailyFields = this.focusedDailyResults
    }

    @action
    shouldSprintComplete = () => {
        this.focusedSprint.status = this.focusedSprint.completedDays == this.focusedSprint.totalDays
            ? 'completed'
            : 'ongoing'
    }

    @action
    saveDaily = () => {
        let currentSprint = this.focusedSprint
        this.focusedDailyResults = this.dailyFields
        if (this.focusedDaily.status == 'ongoing') {
            this.focusedDaily.status = 'completed'
            currentSprint.completedDays++
        }
        if (currentSprint.status == 'ongoing') {
            this.shouldSprintComplete()
        }

        this.focusedSprint = currentSprint
    }

    @action
    cancelDaily = (index) => {
        this.focusedSprint.completedDays++
        this.focusedSprintDailies[index].status = 'cancelled'
        this.iFocusedDaily = index
        const defaultDailyStats = {
            pto: false,
            mood: 'N/A',
            presence: 'N/A',
            board: 'N/A',
            video: 'N/A'
        }
        this.generateDailyForm(defaultDailyStats)
        this.saveDaily()
        this.shouldSprintComplete()
    }

    @action
    reopenDaily = (index) => {
        this.focusedSprint.completedDays--
        this.focusedSprintDailies[index].status = 'ongoing'
        this.iFocusedDaily = index
        this.focusedDailyResults = []
        this.shouldSprintComplete()
    }

    @action
    saveProjects = (onSuccess = () => {}, onFail = () => {}) => {
        let uid = firebase.auth().currentUser.uid
        let projects = this.projects
        firebase.database().ref(`users/${uid}/projects`).set(projects)
        .then(() => {
            onSuccess()
        })
        .catch(error => {
            /* Treat any exceptions here */
            showMessage({
                type: 'danger',
                message: strings.firebaseAuth[error.code].code,
                description: strings.firebaseAuth[error.code].message,
            })
            onFail(error)
        })
    }

    @action
    loadProjects = () => {
        let uid = firebase.auth().currentUser.uid
        firebase.database().ref(`users/${uid}`).once('value')
        .then( snapshot => {
            let updatedProjects = snapshot.val().projects
            this.projects = updatedProjects || []
        })
        .catch( error => {
            /* Treat any exceptions here */
            console.log(error)
            showMessage({
                type: 'danger',
                message: strings.firebaseAuth[error.code].code,
                description: strings.firebaseAuth[error.code].message,
            })
        })
    }
    
    @action
    generateProjectsReport = (title, message, subject) => {
        const DATA_TYPE = 'data:file/csv;base64,'
        let csv = 'Produto;Pessoa;Time;MonthlyRelease;Sprint;Dia;Valido;Presence;Video;Jira;Mood\n'
        this.projects.forEach(project=> {
            if (project.releases) project.releases.forEach(release => {
                if (release.sprints) release.sprints.forEach(sprint => {
                    if (sprint.dailies) sprint.dailies.forEach(daily => {
                        if (daily.results) daily.results.forEach(result => {
                            csv += this.getCsvResultString(project.name, result.name, formatDate(release.date, false), sprint.id+1, daily, result)
                        })
                    })
                })
            })
        })

        this.shareUrl(DATA_TYPE, csv, title, message, subject)
    }

    getCsvResultString = (product, person, mr, sprintId, daily, result) => {
        return `${product};${person};N/A;${mr};${sprintId};${daily.id+1};${daily.status};${result.presence};${result.video};${result.board};${result.mood}\n`
    }

    @action
    shareUrl = (dataType, url, title, message, subject) => {
        const URL_BASE64 = dataType + base64.encode(url)

        let shareBase64 = {
            ...Platform.select({
                android: {
                    title: title,
                    message: message
                }
            }),
            url: URL_BASE64,
            subject: subject
        }
    
        Share.open(shareBase64).then(res => {
              console.log(res)
        })
        .catch(err => {
              err && console.log(err)
        })
    }
}