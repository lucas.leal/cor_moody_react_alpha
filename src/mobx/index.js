import AppStore from './stores/appStore'
import UserStore from './stores/userStore';

export default {
    appStore: new AppStore(),
    userStore: new UserStore()
}